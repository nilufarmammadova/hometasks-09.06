--Set 3
--Create a table: Create a table named 'Project_Details' with columns 'Project_ID' (number type),
--'Project_Name' (varchar type), 'Start_Date' (date type), 'End_Date' (date type) and
--'Manager_ID' (number type).
CREATE TABLE project_details (
    project_id   NUMBER,
    project_name VARCHAR2(50),
    start_date   DATE,
    end_date     DATE,
    manager_id   NUMBER
);

--Delete operation: Delete from 'Employees' where 'first_name' is 'John' and 'last_name' is 'Doe'.
DELETE FROM employees
WHERE
        first_name = 'John'
    AND last_name = 'Doe';
    
--Select with sorting: Write a SELECT statement to get all the employees in descending order of hire date.
SELECT
    *
FROM
    employees
ORDER BY
    hire_date DESC;
    
--Number Function: Write a query using the ROUND function to round off the salaries in the 'Employees' table
--to the nearest hundred.
SELECT
    salary,
    round(salary, - 2)
FROM
    employees;
    
--Right Join: Write a query to get all departments and their employees, including departments
--without employees.
SELECT
    employee_id,
    department_name
FROM
    employees e
RIGHT JOIN departments d ON e.department_id = d.department_id;
