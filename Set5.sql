--Set 5
--Drop a table: Drop the 'Project_Details' table.
DROP TABLE project_details;

--Insert operation with select: Insert into 'Employee_Details' table all the employees from 'Employees' table
--where the hire date is after '01-JAN-2021'.
INSERT INTO employee_details (id, name, dob, department_id, salary)
SELECT
    employee_id,
    first_name,
    hire_date,
    department_id,
    salary
FROM
    employees
WHERE
    hire_date > '01-jan-2021';

--Select with group by: Write a SELECT statement to get the job_id and the number of employees working in
--each job_id from the 'Employees' table.
SELECT
    job_id,
    COUNT(employee_id)
FROM
    employees
GROUP BY
    job_id;

--Analytical Function: Write a query using the RANK function to return the salaries and rank of
--each employee based on their salary within their department.
SELECT
    employee_id,
    salary,
    department_id,
    RANK()
    OVER(PARTITION BY department_id
         ORDER BY
             salary DESC
    ) AS salary_rank
FROM
    employees;

--Set operator: Write a query using UNION to return the job_ids from both the 'Jobs' and 'Job_History'
--tables.
SELECT
    job_id
FROM
    jobs union
SELECT
    job_id
FROM
    job_history;
