--Set 1
--Create a table: Create a table named 'Employee_Details'
--with columns 'ID' (number type), 'Name' (varchar type), 'DOB' (date type), and
--'Department_ID' (number type).
CREATE TABLE employee_details (
    id            NUMBER,
    name          VARCHAR2(50),
    dob           DATE,
    department_id NUMBER
); 

--Insert operation: Insert a row into the 'Employees' table where first_name is 'John', last_name is 'Doe',
--email is 'jdoe@mail.com', and hire_date is '15-JAN-2022'.
INSERT INTO employees (first_name, last_name, email, hire_date) VALUES ( 'John',
'Doe',
'jdoe@mail.com',
'15-JAN-2022' ); 

--Select with condition: Write a SELECT statement to get the details of all the employees
--who were hired after '01-JAN-2020'.
SELECT
    *
FROM
    employees
WHERE
    hire_date > '01-01-2020';


--Character Function: Write a query using the INITCAP function to return all job titles
--from the 'Jobs' table with the first letter capitalized.
SELECT
    initcap(job_title)
FROM
    jobs;

--Inner Join: Write a query to join the 'Employees' and 'Departments' tables to get the department names
--for all employees.
SELECT
    employee_id,
    department_name
FROM
    employees e
JOIN departments d ON e.department_id = d.department_id;
